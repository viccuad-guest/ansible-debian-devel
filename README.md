Role Name
=========

Ansible role for setting up a development environment of Debian packages and
infrastructure.

Requirements
------------

None.

Role Variables
--------------

TODO

Dependencies
------------

None.

Example Playbook
----------------

See the included debian-devel.yml playbook.

For testing locally, a Vagrantile for libvirt is provided. Just do
a `vagrant up` and you will get a fully configured Debian instance
for development.

License
-------

GPL-3
